import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-resetear',
  templateUrl: './resetear.component.html',
  styleUrls: ['./resetear.component.css']
})
export class ResetearComponent implements OnInit {

  @Input() contador: number = 0;
  @Output() contadorChange = new EventEmitter<number>();

 reset(){
  this.contador = 0;
  this.contadorChange.emit(this.contador);
 }
 ngOnInit(): void {
}
emitirDatoAlPadre(event:number){
  this.contador = event;
  this.contadorChange.emit(this.contador);
}
}
