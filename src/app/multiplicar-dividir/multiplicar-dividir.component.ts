import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multiplicar-dividir',
  templateUrl: './multiplicar-dividir.component.html',
  styleUrls: ['./multiplicar-dividir.component.css'],
})
export class MultiplicarDividirComponent implements OnInit {
  @Input() contador: number = 0;
  @Output() contadorChange = new EventEmitter<number>();

  multiplicar() {
    this.contador = this.contador * 2;
    this.contadorChange.emit(this.contador);
  }
  dividir() {
    this.contador = this.contador / 2;
    this.contadorChange.emit(this.contador);
  }
  emitirDatoAlPadre(event:number){
    this.contador = event;
    this.contadorChange.emit(this.contador);
  }
  ngOnInit() {}
}
