import { Component, Input,Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  // contador: number = 0;
  @Input() contador: number = 0;

  // @Output() contadorChange = new EventEmitter<number>();


  asignCont(cont: number) {
    this.contador = cont;
  }
}
