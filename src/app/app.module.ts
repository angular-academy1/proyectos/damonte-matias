import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IncrementarDecrementarComponent } from './incrementar-decrementar/incrementar-decrementar.component';
import { MultiplicarDividirComponent } from './multiplicar-dividir/multiplicar-dividir.component';
import { ResetearComponent } from './resetear/resetear.component';
import { FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    IncrementarDecrementarComponent,
    MultiplicarDividirComponent,
    ResetearComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
