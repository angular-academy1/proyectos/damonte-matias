import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-incrementar-decrementar',
  templateUrl: './incrementar-decrementar.component.html',
  styleUrls: ['./incrementar-decrementar.component.css']
})
export class IncrementarDecrementarComponent implements OnInit {

  @Input() contador: number = 0;
  @Output() contadorChange = new EventEmitter<number>();


  constructor() { }


  ngOnInit(): void {
  }

  sumar(){
    this.operacion(+1);
    }
  restar(){
    this.operacion(-1);
  }

  operacion(cont: number){
    this.contador = this.contador + cont;
    this.contadorChange.emit(this.contador);
  }

  emitirDatoAlPadre(event:number){
    this.contador = event;
    this.contadorChange.emit(this.contador);
  }

}
